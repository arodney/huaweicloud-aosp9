/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 * Description: jni helper function
 */

void HwMountEmulatedStorage(int runtime_flags);
