/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
 * Description: installd helper function
 */

void fix_app_path(const char *, uid_t, gid_t);

